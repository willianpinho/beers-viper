//
//  BeerModel.swift
//  beers-viper
//
//  Created by Willian Pinho on 11/12/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class BeerModel: Object, Mappable {
    @objc dynamic var id = 0
    @objc dynamic var name: String?
    @objc dynamic var tagline: String?
    @objc dynamic var descriptionBeer: String?
    @objc dynamic var imageUrl: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(id: Int?, name: String? , tagline: String?, descriptionBeer: String?, imageUrl: String?) {
        self.init()
        
        self.id = id!
        self.name = name
        self.tagline = tagline
        self.descriptionBeer = descriptionBeer
        self.imageUrl = imageUrl
    }
    
    required convenience init?(map: Map) { self.init() }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        tagline <- map["tagline"]
        descriptionBeer <- map["description"]
        imageUrl <- map["image_url"]
    }
        
}


