//
//  BeerDetailView.swift
//  beers-viper
//
//  Created by Willian Pinho on 11/12/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation
import UIKit

class BeerDetailView: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tagLineLabel: UILabel!
    @IBOutlet weak var descriptionBeerLabel: UILabel!
    @IBOutlet weak var beerImageView: UIImageView!
    
    var presenter: BeerDetailPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
}

extension BeerDetailView: BeerDetailViewProtocol {
    
    func showBeerDetail(forBeer beer: BeerModel) {
        self.title = beer.name
        nameLabel?.text = beer.name
        tagLineLabel.text = beer.tagline
        descriptionBeerLabel.text = beer.descriptionBeer
        let url = URL(string: beer.imageUrl!)!
        let placeholderImage = UIImage(named: "placeholder")!
        beerImageView?.af_setImage(withURL: url, placeholderImage: placeholderImage)
    }
    
}
