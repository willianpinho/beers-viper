//
//  BeerDetailWireframe.swift
//  beers-viper
//
//  Created by Willian Pinho on 11/12/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation
import UIKit

class BeerDetailWireFrame: BeerDetailWireFrameProtocol {
    
    class func createBeerDetailModule(forBeer beer: BeerModel) -> UIViewController {
        let viewController = beerDetailStoryboard.instantiateViewController(withIdentifier: "BeerDetailView")
        if let view = viewController as? BeerDetailView {
            let presenter: BeerDetailPresenterProtocol = BeerDetailPresenter()
            let wireFrame: BeerDetailWireFrameProtocol = BeerDetailWireFrame()
            
            view.presenter = presenter
            presenter.view = view
            presenter.beer = beer
            presenter.wireFrame = wireFrame
            
            return viewController
        }
        
        return UIViewController()
        
    }
    
    static var beerDetailStoryboard: UIStoryboard {
        return UIStoryboard(name: "BeerDetail", bundle: Bundle.main)
    }
}
