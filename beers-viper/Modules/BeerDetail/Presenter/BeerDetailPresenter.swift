//
//  BeerDetailPresenter.swift
//  beers-viper
//
//  Created by Willian Pinho on 11/12/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation

class BeerDetailPresenter: BeerDetailPresenterProtocol {
    weak var view: BeerDetailViewProtocol?
    var wireFrame: BeerDetailWireFrameProtocol?
    var beer:  BeerModel?
    
    func viewDidLoad() {
        view?.showBeerDetail(forBeer: beer!)
    }
}
