//
//  BeerDetailProtocol.swift
//  beers-viper
//
//  Created by Willian Pinho on 11/12/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation
import UIKit

protocol BeerDetailViewProtocol: class {
    var presenter: BeerDetailPresenterProtocol? { get set }
    
    func showBeerDetail(forBeer beer: BeerModel)
}

protocol BeerDetailWireFrameProtocol: class {
    static func createBeerDetailModule(forBeer beer: BeerModel) -> UIViewController
}

protocol BeerDetailPresenterProtocol: class {
    var view: BeerDetailViewProtocol? { get set }
    var wireFrame: BeerDetailWireFrameProtocol? { get set }
    var beer: BeerModel? { get set }
    
    func viewDidLoad()
}
