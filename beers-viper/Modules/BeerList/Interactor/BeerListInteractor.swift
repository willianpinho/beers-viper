//
//  BeerListInteractor.swift
//  beers-viper
//
//  Created by Willian Pinho on 11/12/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation

class BeerListInteractor: BeerListInteractorInputProtocol {
    weak var presenter: BeerListInteractorOutputProtocol?
    var localDataManager: BeerListLocalDataManagerInputProtocol?
    var remoteDataManager: BeerListRemoteDataManagerInputProtocol?
    
    func retrieveBeerList() {
        do {
            if let beerList = try localDataManager?.retrieveBeerList() {
                let beerModelList = beerList.map() {
                    return BeerModel(id: Int($0.id), name: $0.name, tagline: $0.tagline, descriptionBeer: $0.descriptionBeer, imageUrl: $0.imageUrl)
                }
                if beerModelList.isEmpty {
                    remoteDataManager?.retrieveBeerList()
                } else {
                    presenter?.didRetrieveBeers(beerModelList)
                }
            }
        } catch {
            presenter?.didRetrieveBeers([])
        }
    }
}

extension BeerListInteractor: BeerListRemoteDataManagerOutputProtocol {
    func onBeersRetrieved(_ beers: [BeerModel]) {
        presenter?.didRetrieveBeers(beers)
        
        for beerModel in beers {
            do {
                try localDataManager?.saveBeer(id: beerModel.id, name: beerModel.name!, tagline: beerModel.tagline!, descriptionBeer: beerModel.descriptionBeer!, imageUrl: beerModel.imageUrl!)
            } catch {
                
            }
        }
    }
    
    func onError() {
        presenter?.onError()
    }
    
    
}
