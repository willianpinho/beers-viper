//
//  BeerListPresenter.swift
//  beers-viper
//
//  Created by Willian Pinho on 11/12/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation

class BeerListPresenter: BeerListPresenterProtocol {
    
    weak var view: BeerListViewProtocol?
    var interactor: BeerListInteractorInputProtocol?
    var wireFrame: BeerListWireFrameProtocol?
    
    func viewDidLoad() {
        view?.showLoading()
        view?.registerTableViewCell()
        interactor?.retrieveBeerList()
    }
    
    func showBeerDetail(forBeer beer: BeerModel) {
        wireFrame?.presentBeerDetailScreen(from: view!, forBeer: beer)
    }
    
}

extension BeerListPresenter: BeerListInteractorOutputProtocol {
    func didRetrieveBeers(_ beers: [BeerModel]) {
        view?.hideLoading()
        view?.showBeers(with: beers)
    }
    
    func onError() {
        view?.hideLoading()
        view?.showError()
    }
}
