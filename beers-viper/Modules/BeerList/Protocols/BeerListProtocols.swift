//
//  BeerListProtocols.swift
//  beers-viper
//
//  Created by Willian Pinho on 11/12/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation
import UIKit

protocol BeerListViewProtocol: class {
    var presenter: BeerListPresenterProtocol? { get set }
    func showBeers(with beers: [BeerModel])
    func showError()
    func showLoading()
    func hideLoading()
    func registerTableViewCell()
}

protocol BeerListWireFrameProtocol: class {
    static func createBeerListModule() -> UIViewController
    func presentBeerDetailScreen(from view: BeerListViewProtocol, forBeer beer: BeerModel)
}

protocol BeerListPresenterProtocol: class {
    var view: BeerListViewProtocol? { get set }
    var interactor: BeerListInteractorInputProtocol? { get set }
    var wireFrame: BeerListWireFrameProtocol? { get set }
    
    func viewDidLoad()
    func showBeerDetail(forBeer beer: BeerModel)
}

protocol BeerListInteractorOutputProtocol: class {
    func didRetrieveBeers(_ beers: [BeerModel])
    func onError()
}

protocol BeerListInteractorInputProtocol: class {
    var presenter: BeerListInteractorOutputProtocol? { get set }
    var localDataManager: BeerListLocalDataManagerInputProtocol? { get set }
    var remoteDataManager: BeerListRemoteDataManagerInputProtocol? { get set }
    
    func retrieveBeerList()
}

protocol BeerListDataManagerInputProtocol: class {
    
}

protocol BeerListRemoteDataManagerInputProtocol: class {
    var remoteRequestHandler: BeerListRemoteDataManagerOutputProtocol? { get set }
    func retrieveBeerList()
}

protocol BeerListRemoteDataManagerOutputProtocol: class {
    func onBeersRetrieved(_ beers: [BeerModel])
    func onError()
}

protocol BeerListLocalDataManagerInputProtocol: class {
    func retrieveBeerList() throws -> [BeerModel]
    func saveBeer(id: Int, name: String, tagline: String, descriptionBeer: String, imageUrl: String) throws
}
