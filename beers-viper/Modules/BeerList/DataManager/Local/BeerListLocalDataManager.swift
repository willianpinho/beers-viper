//
//  BeerListLocalDataManager.swift
//  beers-viper
//
//  Created by Willian Pinho on 11/12/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation
import Realm

class BeerListLocalDataManager: BeerListLocalDataManagerInputProtocol {
    func saveBeer(id: Int, name: String, tagline: String, descriptionBeer: String, imageUrl: String) throws {
        
        ApplicationState.sharedInstance.realm.beginWrite()
        let beer = BeerModel(id: id, name: name, tagline: tagline, descriptionBeer: descriptionBeer, imageUrl: imageUrl)
        ApplicationState.sharedInstance.realm.add(beer)
        try ApplicationState.sharedInstance.realm.commitWrite()
    }
    
    func retrieveBeerList() throws -> [BeerModel] {
        return Array(ApplicationState.sharedInstance.realm.objects(BeerModel.self))
    }
    
    func saveBeer(beer: BeerModel?) {
        if beer != nil {
        } else {
            print("Can't Save beer")
        }
    }
}
