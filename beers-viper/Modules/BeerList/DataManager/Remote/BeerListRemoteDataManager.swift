//
//  BeerListRemoteDataManager.swift
//  beers-viper
//
//  Created by Willian Pinho on 11/12/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class BeerListRemoteDataManager: BeerListRemoteDataManagerInputProtocol {
    
    var remoteRequestHandler: BeerListRemoteDataManagerOutputProtocol?
    
    func retrieveBeerList() {
        Alamofire
            .request(EndPoints.Beers.fetch.url, method: .get)
            .validate()
            .responseArray(completionHandler: { (response: DataResponse<[BeerModel]>) in
                switch response.result {
                case .success(let beers):
                    self.remoteRequestHandler?.onBeersRetrieved(beers)
                case .failure( _):
                    self.remoteRequestHandler?.onError()
                }
            })
    }
    
}
