//
//  BeerListTableViewCell.swift
//  beers-viper
//
//  Created by Willian Pinho on 11/12/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import UIKit
import AlamofireImage

class BeerListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tagLineLabel: UILabel!
    @IBOutlet weak var beerImageView: UIImageView!

    func set(forBeer beer: BeerModel) {
        nameLabel.text = beer.name
        tagLineLabel.text = beer.tagline
        let url = URL(string: beer.imageUrl!)!
        let placeholderImage = UIImage(named: "placeholder")!
        beerImageView?.af_setImage(withURL: url, placeholderImage: placeholderImage)
    }
    
}
