//
//  BeerListView.swift
//  beers-viper
//
//  Created by Willian Pinho on 11/12/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation
import UIKit
import PKHUD

class BeerListView: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var presenter: BeerListPresenterProtocol?
    var beerList: [BeerModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
}

extension BeerListView: BeerListViewProtocol {
    func showBeers(with beers: [BeerModel]) {
        beerList = beers
        tableView.reloadData()
    }
    
    func showError() {
        HUD.flash(.label("Internet not connected"), delay: 1.5)
    }
    
    func showLoading() {
        HUD.show(.progress)
    }
    
    func hideLoading() {
        HUD.hide()
    }
    
    func registerTableViewCell() {
        tableView.register(UINib(nibName: "BeerListTableViewCell", bundle: nil), forCellReuseIdentifier: "BeerListTableViewCell")
    }
}

extension BeerListView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BeerListTableViewCell", for: indexPath) as! BeerListTableViewCell
        
        let beer = beerList[indexPath.row]
        cell.set(forBeer: beer)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return beerList.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.showBeerDetail(forBeer: beerList[indexPath.row])
    }
}
