//
//  BeerListWireFrame.swift
//  beers-viper
//
//  Created by Willian Pinho on 11/12/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation
import UIKit
class BeerListWireFrame: BeerListWireFrameProtocol {
    class func createBeerListModule() -> UIViewController {
        let navController = beerListStoryBoard.instantiateViewController(withIdentifier: "BeersNavigationController")
        if let view = navController.childViewControllers.first as? BeerListView {
            let presenter: BeerListPresenterProtocol & BeerListInteractorOutputProtocol = BeerListPresenter()
            let interactor: BeerListInteractorInputProtocol & BeerListRemoteDataManagerOutputProtocol = BeerListInteractor()
            let localDataManager: BeerListLocalDataManagerInputProtocol = BeerListLocalDataManager()
            let remoteDataManager: BeerListRemoteDataManagerInputProtocol = BeerListRemoteDataManager()
            let wireFrame: BeerListWireFrameProtocol = BeerListWireFrame()
            
            view.presenter = presenter
            presenter.view = view
            presenter.wireFrame = wireFrame
            presenter.interactor = interactor
            interactor.presenter = presenter
            interactor.localDataManager = localDataManager
            interactor.remoteDataManager = remoteDataManager
            remoteDataManager.remoteRequestHandler = interactor
            
            return navController
        }
        
        return UIViewController()
    }
    
    static var beerListStoryBoard: UIStoryboard {
        return UIStoryboard(name: "BeerList", bundle: Bundle.main)
    }
    
    func presentBeerDetailScreen(from view: BeerListViewProtocol, forBeer beer: BeerModel) {
        let beerDetailViewController = BeerDetailWireFrame.createBeerDetailModule(forBeer: beer)
        
        if let sourceView = view as? UIViewController {
            sourceView.navigationController?.pushViewController(beerDetailViewController, animated: true)
        }
    }

}
