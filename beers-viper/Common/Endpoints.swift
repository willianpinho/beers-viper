//
//  Endpoints.swift
//  beers-viper
//
//  Created by Willian Pinho on 11/12/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation

struct API {
    static let baseUrl = "https://api.punkapi.com/v2"
}

protocol EndPoint {
    var path: String { get }
    var url: String { get }
}

enum EndPoints {
    enum Beers: EndPoint {
        case fetch
        
        public var path: String {
            switch self {
            case .fetch: return "/beers"
            }
        }
        
        public var url: String {
            switch self {
            case .fetch: return "\(API.baseUrl)\(path)"
            }
        }
    }
}
