//
//  ApplicationState.swift
//  beers-viper
//
//  Created by Willian Pinho on 11/12/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class ApplicationState {
    static let sharedInstance = ApplicationState()
    let realm = try! Realm()

}
