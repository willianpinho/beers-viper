//
//  PersistenceError.swift
//  beers-viper
//
//  Created by Willian Pinho on 11/12/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation

enum PersistenceError: Error {
    case realmNotFound
    case couldNotSaveObject
    case objectNotFound
}
